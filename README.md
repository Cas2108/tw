#Image Classification API using Tensorflow 

API for image classification using Tensorflow and Deeplearning

To build the project on your machine, open the terminal and run 'sudo docker-compose build' 
and 'sudo docker-compose up' in the project directory

Download the inceptionv3 model and add it to your project  : http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz

For testing the API install Postman->create POST requests at 'localhost5000:/register' , 'classify' , '/refill'



